import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MytasksRoutingModule } from '@app/mytasks/mytasks-routing.module';
import { MytasksComponent } from '@app/mytasks/mytasks.component';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    MytasksRoutingModule
  ],
  declarations: [MytasksComponent]
})
export class MytasksModule { }
