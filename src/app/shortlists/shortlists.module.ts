import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShortlistsComponent } from './shortlists.component';
import { ShortListsRoutingModule } from '@app/shortlists/shortlists-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    ShortListsRoutingModule
  ],
  declarations: [ShortlistsComponent]
})
export class ShortlistsModule { }
