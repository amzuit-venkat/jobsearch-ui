import { ShortlistsModule } from './shortlists.module';

describe('ShortlistsModule', () => {
  let shortlistsModule: ShortlistsModule;

  beforeEach(() => {
    shortlistsModule = new ShortlistsModule();
  });

  it('should create an instance', () => {
    expect(shortlistsModule).toBeTruthy();
  });
});
